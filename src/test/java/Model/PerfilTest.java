/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Registos.ListaPeriodoAutorizacao;
import Utils.Data;
import Utils.Tempo;
import java.io.File;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ana Isabel Fonseca
 */
public class PerfilTest {

    public PerfilTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of isEquipamentoAutorizado method, of class Perfil.
     */
    @Test
    public void testIsEquipamentoAutorizado() {
        System.out.println("isEquipamentoAutorizado");
        Equipamento eq = new Equipamento("192.168.1.254", "127.0.0.1", "Teste", new File("Ficheiro"), "entrada");
        Data data = Data.dataAtual();
        Tempo hora = Tempo.tempoAtual();
        ListaPeriodoAutorizacao listaPA = new ListaPeriodoAutorizacao();
        listaPA.criaPeriodo(eq, Data.dataAtual().diaDaSemana(), new Tempo(Tempo.tempoAtual().getHoras() - 1), new Tempo(Tempo.tempoAtual().getHoras() + 2));
        Perfil instance = new Perfil("1234", "Teste", listaPA);
        boolean expResult = true;
        boolean result = instance.isEquipamentoAutorizado(eq, data, hora);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Perfil.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        ListaPeriodoAutorizacao listaPA = new ListaPeriodoAutorizacao();
        listaPA.criaPeriodo(new Equipamento("192.168.1.254", "127.0.0.1", "Teste", new File("Ficheiro"), "entrada"), Data.dataAtual().diaDaSemana(), new Tempo(Tempo.tempoAtual().getHoras() - 1), new Tempo(Tempo.tempoAtual().getHoras() + 2));
        Perfil outroPerfil = new Perfil("1234", "Teste", listaPA);
        Perfil instance = new Perfil("1234", "Teste", listaPA);
        boolean expResult = true;
        boolean result = instance.equals(outroPerfil);
        assertEquals(expResult, result);
    }

    /**
     * Test of getId method, of class Perfil.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        ListaPeriodoAutorizacao listaPA = new ListaPeriodoAutorizacao();
        listaPA.criaPeriodo(new Equipamento("192.168.1.254", "127.0.0.1", "Teste", new File("Ficheiro"), "entrada"), Data.dataAtual().diaDaSemana(), new Tempo(Tempo.tempoAtual().getHoras() - 1), new Tempo(Tempo.tempoAtual().getHoras() + 2));
        Perfil instance = new Perfil("1234", "Teste", listaPA);
        String expResult = "1234";
        String result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDescricao method, of class Perfil.
     */
    @Test
    public void testGetDescricao() {
        System.out.println("getDescricao");
        ListaPeriodoAutorizacao listaPA = new ListaPeriodoAutorizacao();
        listaPA.criaPeriodo(new Equipamento("192.168.1.254", "127.0.0.1", "Teste", new File("Ficheiro"), "entrada"), Data.dataAtual().diaDaSemana(), new Tempo(Tempo.tempoAtual().getHoras() - 1), new Tempo(Tempo.tempoAtual().getHoras() + 2));
        Perfil instance = new Perfil("1234", "Teste", listaPA);
        String expResult = "Teste";
        String result = instance.getDescricao();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaPA method, of class Perfil.
     */
    @Test
    public void testGetListaPA() {
        System.out.println("getListaPA");
        ListaPeriodoAutorizacao listaPA = new ListaPeriodoAutorizacao();
        listaPA.criaPeriodo(new Equipamento("192.168.1.254", "127.0.0.1", "Teste", new File("Ficheiro"), "entrada"), Data.dataAtual().diaDaSemana(), new Tempo(Tempo.tempoAtual().getHoras() - 1), new Tempo(Tempo.tempoAtual().getHoras() + 2));
        Perfil instance = new Perfil("1234", "Teste", listaPA);
        ListaPeriodoAutorizacao expResult = listaPA;
        ListaPeriodoAutorizacao result = instance.getListaPA();
        assertEquals(expResult, result);
    }

}
