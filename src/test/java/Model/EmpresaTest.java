/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Registos.RegistoCartoes;
import Registos.RegistoColaboradores;
import Registos.RegistoEquipamentos;
import java.io.File;
import java.util.ArrayList;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ana Isabel Fonseca
 */
public class EmpresaTest {
    
    public EmpresaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of instanciarRegistos method, of class Empresa.
     */
    @Test
    public void testInstanciarRegistos() throws Exception {
        System.out.println("instanciarRegistos");
        String texto1 = "Equip/192.168.1.254/127.0.0.1/Teste/Ficheiro/entrada";
        String[] linha = texto1.trim().split("/");
        Empresa.instanciarRegistos(linha);
        Equipamento expResult = new Equipamento("192.168.1.254", "127.0.0.1", "Teste", new File("Ficheiro"), "entrada");
        assertEquals(expResult.toString(), RegistoEquipamentos.getListaEquipamentos().get(0).toString());
    }

    /**
     * Test of setRegistos method, of class Empresa.
     */
    @Test
    public void testSetRegistos() {
        System.out.println("setRegistos");
        ArrayList<Colaborador> registoColab = new ArrayList<>();
        registoColab.add(new Colaborador("1234", "Tiago Ribeiro"));
        ArrayList<Equipamento> registoEquip = new ArrayList<>();
        registoEquip.add(new Equipamento("192.168.1.254", "127.0.0.1", "Teste", new File("Ficheiro"), "entrada"));
        ArrayList<CartaoProvisorio> registoCartoesP = new ArrayList<>();
        registoCartoesP.add(new CartaoProvisorio("3412", "666", "10"));
        ArrayList<CartaoDefinitivo> registoCartoesD = new ArrayList<>();
        registoCartoesD.add(new CartaoDefinitivo("5678", "1234"));
        Empresa.setRegistos(registoColab, registoEquip, registoCartoesP, registoCartoesD);
        assertEquals(registoColab, RegistoColaboradores.getListaColab());
        assertEquals(registoEquip, RegistoEquipamentos.getListaEquipamentos());
        assertEquals(registoCartoesP, RegistoCartoes.getRegistoCartoesP());
        assertEquals(registoCartoesD, RegistoCartoes.getRegistoCartoesD());
    }
    
}
