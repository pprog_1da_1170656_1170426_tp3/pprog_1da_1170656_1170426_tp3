/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Registos.ListaPeriodoAutorizacao;
import Utils.Data;
import Utils.Tempo;
import java.io.File;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Ana Isabel Fonseca
 */
public class PeriodoAutorizacaoTest {
    
    public PeriodoAutorizacaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getEquipamento method, of class PeriodoAutorizacao.
     */
    @Ignore
    public void testGetEquipamento() {
        System.out.println("getEquipamento");
        PeriodoAutorizacao instance = null;
        Equipamento expResult = null;
        Equipamento result = instance.getEquipamento();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getHoraInicio method, of class PeriodoAutorizacao.
     */
    @Ignore
    public void testGetHoraInicio() {
        System.out.println("getHoraInicio");
        PeriodoAutorizacao instance = null;
        Tempo expResult = null;
        Tempo result = instance.getHoraInicio();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getHoraFim method, of class PeriodoAutorizacao.
     */
    @Ignore
    public void testGetHoraFim() {
        System.out.println("getHoraFim");
        PeriodoAutorizacao instance = null;
        Tempo expResult = null;
        Tempo result = instance.getHoraFim();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDiaSemana method, of class PeriodoAutorizacao.
     */
    @Ignore
    public void testGetDiaSemana() {
        System.out.println("getDiaSemana");
        PeriodoAutorizacao instance = null;
        String expResult = "";
        String result = instance.getDiaSemana();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of equals method, of class PeriodoAutorizacao.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        PeriodoAutorizacao outroPA = new PeriodoAutorizacao (new Equipamento("192.168.1.254", "127.0.0.1", "Teste", new File("Ficheiro"), "entrada"), Data.dataAtual().diaDaSemana(), new Tempo(Tempo.tempoAtual().getHoras() - 1), new Tempo(Tempo.tempoAtual().getHoras() + 2));
        PeriodoAutorizacao instance = new PeriodoAutorizacao (new Equipamento("192.168.1.254", "127.0.0.1", "Teste", new File("Ficheiro"), "entrada"), Data.dataAtual().diaDaSemana(), new Tempo(Tempo.tempoAtual().getHoras() - 1), new Tempo(Tempo.tempoAtual().getHoras() + 2));
        boolean expResult = true;
        boolean result = instance.equals(outroPA);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class PeriodoAutorizacao.
     */
    @Ignore
    public void testToString() {
        System.out.println("toString");
        PeriodoAutorizacao instance = null;
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
