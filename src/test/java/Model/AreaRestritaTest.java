/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Ana Isabel Fonseca
 */
public class AreaRestritaTest {
    
    public AreaRestritaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of lotacaoPermiteAcesso method, of class AreaRestrita.
     */
    @Test
    public void testLotacaoPermiteAcesso1() {
        System.out.println("lotacaoPermiteAcesso - CASO 1");
        String mov = "entrada";
        AreaRestrita instance = new AreaRestrita("teste1", 2);
        boolean expResult = true;
        boolean result = instance.lotacaoPermiteAcesso(mov);
        assertEquals(expResult, result);
    }
    

    /**
     * Test of toString method, of class AreaRestrita.
     */
    @Ignore
    public void testToString() {
        System.out.println("toString");
        AreaRestrita instance = new AreaRestrita();
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
