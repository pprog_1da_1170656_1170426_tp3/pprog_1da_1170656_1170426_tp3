/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Registos;

import Model.Equipamento;
import Model.PeriodoAutorizacao;
import Utils.Data;
import Utils.Tempo;
import java.io.File;
import java.util.ArrayList;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Ana Isabel Fonseca
 */
public class ListaPeriodoAutorizacaoTest {

    public ListaPeriodoAutorizacaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of criaPeriodo method, of class ListaPeriodoAutorizacao.
     */
    @Test
    public void testCriaPeriodo() {
        System.out.println("criaPeriodo");
        ListaPeriodoAutorizacao instance = new ListaPeriodoAutorizacao();
        boolean expResult = true;
        boolean result = instance.criaPeriodo(new Equipamento("192.168.1.254", "127.0.0.1", "Teste", new File("Ficheiro"), "entrada"), Data.dataAtual().diaDaSemana(), new Tempo(Tempo.tempoAtual().getHoras() - 1), new Tempo(Tempo.tempoAtual().getHoras() + 2));

        assertEquals(expResult, result);
    }

    /**
     * Test of addPeriodo method, of class ListaPeriodoAutorizacao.
     */
    @Test
    public void testAddPeriodo() {
        System.out.println("addPeriodo");
        PeriodoAutorizacao pa = null;
        ListaPeriodoAutorizacao instance = new ListaPeriodoAutorizacao();
        instance.addPeriodo(pa);
    }

    /**
     * Test of valida method, of class ListaPeriodoAutorizacao.
     * Esta validação é testada na criação do Período de Autorização.
     */
    @Ignore
    public void testValida() {
        System.out.println("valida");
        PeriodoAutorizacao pa = null;
        ListaPeriodoAutorizacao instance = new ListaPeriodoAutorizacao();
        boolean expResult = false;
        boolean result = instance.valida(pa);
        assertEquals(expResult, result);
    }

    /**
     * Test of getLista method, of class ListaPeriodoAutorizacao.
     */
    @Ignore
    public void testGetLista() {
        System.out.println("getLista");
        ListaPeriodoAutorizacao instance = new ListaPeriodoAutorizacao();
        ArrayList<PeriodoAutorizacao> expResult = null;
        ArrayList<PeriodoAutorizacao> result = instance.getLista();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}
