/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Registos;

import Model.Colaborador;
import Model.Equipamento;
import Model.Perfil;
import Utils.Data;
import Utils.Tempo;
import java.io.File;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Ana Isabel Fonseca
 */
public class RegistoPerfisTest {
    
    public RegistoPerfisTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of novoPerfil method, of class RegistoPerfis.
     */
    @Ignore
    public void testNovoPerfil() {
        System.out.println("novoPerfil");
        String idPerfil = "12346";
        String descricao = "Teste";
        ListaPeriodoAutorizacao listaPA = new ListaPeriodoAutorizacao();
        listaPA.criaPeriodo(new Equipamento("192.168.1.254", "127.0.0.1", "Teste", new File("Ficheiro"), "entrada"), Data.dataAtual().diaDaSemana(), new Tempo(Tempo.tempoAtual().getHoras() - 1), new Tempo(Tempo.tempoAtual().getHoras() + 2));
        Colaborador colab = new Colaborador("1234", "Tiago Ribeiro");
        boolean expResult = true;
        boolean result = RegistoPerfis.novoPerfil(idPerfil, descricao, listaPA, colab);
        assertEquals(expResult, result);
    }

    /**
     * Test of valida method, of class RegistoPerfis.
     */
    @Ignore
    public void testValida() {
        System.out.println("valida");
        Perfil p = null;
        boolean expResult = false;
        boolean result = RegistoPerfis.valida(p);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addPerfil method, of class RegistoPerfis.
     */
    @Test
    public void testAddPerfil() {
        System.out.println("addPerfil");
        ListaPeriodoAutorizacao listaPA = new ListaPeriodoAutorizacao();
        listaPA.criaPeriodo(new Equipamento("192.168.1.254", "127.0.0.1", "Teste", new File("Ficheiro"), "entrada"), Data.dataAtual().diaDaSemana(), new Tempo(Tempo.tempoAtual().getHoras() - 1), new Tempo(Tempo.tempoAtual().getHoras() + 2));
        Colaborador colab = new Colaborador("1234", "Tiago Ribeiro");
        Perfil p = new Perfil("1234", "Teste", listaPA);
        RegistoPerfis.addPerfil(p);
    }
    
}
