/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Registos;

import Model.Equipamento;
import java.io.File;
import java.util.ArrayList;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Ana Isabel Fonseca
 */
public class RegistoEquipamentosTest {
    
    public RegistoEquipamentosTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getListaEquipamentos method, of class RegistoEquipamentos.
     */
    @Ignore
    public void testGetListaEquipamentos() {
        System.out.println("getListaEquipamentos");
        ArrayList<Equipamento> expResult = null;
        ArrayList<Equipamento> result = RegistoEquipamentos.getListaEquipamentos();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of novoEquipamento method, of class RegistoEquipamentos.
     */
    @Test
    public void testNovoEquipamento() {
        System.out.println("novoEquipamento");
        String endLogico = "192.168.1.254";
        String endFisico = "127.0.0.1";
        String descricao = "Teste";
        File fxConf = new File("Ficheiro");
        String movimento = "entrada";
        RegistoEquipamentos.novoEquipamento(endLogico, endFisico, descricao, fxConf, movimento);
    }

    /**
     * Test of procurarEquipamento method, of class RegistoEquipamentos.
     */
    @Test
    public void testProcurarEquipamento() {
        System.out.println("procurarEquipamento");
        RegistoEquipamentos.novoEquipamento("192.168.1.254", "127.0.0.1", "Teste", new File("Ficheiro"), "entrada");
        String idEquipamento = "192.168.1.254";
        String expResult = new Equipamento("192.168.1.254", "127.0.0.1", "Teste", new File("Ficheiro"), "entrada").toString();
        String result = RegistoEquipamentos.procurarEquipamento(idEquipamento).toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of valida method, of class RegistoEquipamentos.
     */
    @Ignore
    public void testValida() {
        System.out.println("valida");
        Equipamento equip = null;
        boolean expResult = false;
        boolean result = RegistoEquipamentos.valida(equip);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of add method, of class RegistoEquipamentos.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        Equipamento e = new Equipamento("192.168.1.254", "127.0.0.1", "Teste", new File("Ficheiro"), "entrada");
        RegistoEquipamentos.add(e);
    }

    /**
     * Test of setRegistoEquipamentos method, of class RegistoEquipamentos.
     */
    @Ignore
    public void testSetRegistoEquipamentos() {
        System.out.println("setRegistoEquipamentos");
        ArrayList<Equipamento> registoEquipamentos = null;
        RegistoEquipamentos.setRegistoEquipamentos(registoEquipamentos);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
