/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Registos;

import Model.Colaborador;
import java.util.ArrayList;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Ana Isabel Fonseca
 */
public class RegistoColaboradoresTest {
    
    public RegistoColaboradoresTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of novoColaborador method, of class RegistoColaboradores.
     */
    @Test
    public void testNovoColaborador() {
        System.out.println("novoColaborador");
        String numMecanografico = "1234";
        String nomeCompleto = "Tiago Ribeiro";
        RegistoColaboradores.novoColaborador(numMecanografico, nomeCompleto);
    }

    /**
     * Test of add method, of class RegistoColaboradores.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        Colaborador colab = new Colaborador("1234", "Tiago Ribeiro");
        RegistoColaboradores.add(colab);
    }

    /**
     * Test of valida method, of class RegistoColaboradores.
     */
    @Ignore
    public void testValida() {
        System.out.println("valida");
        Colaborador colab = null;
        boolean expResult = false;
        boolean result = RegistoColaboradores.valida(colab);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getColabByID method, of class RegistoColaboradores.
     */
    @Ignore
    public void testGetColabByID() {
        System.out.println("getColabByID");
        String colabID = "1234";
        String expResult = new Colaborador("1234", "Tiago Ribeiro").nomeAbreviado();
        String result = RegistoColaboradores.getColabByID(colabID).nomeAbreviado();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaColab method, of class RegistoColaboradores.
     */
    @Ignore
    public void testGetListaColab() {
        System.out.println("getListaColab");
        ArrayList<Colaborador> expResult = null;
        ArrayList<Colaborador> result = RegistoColaboradores.getListaColab();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of removeColab method, of class RegistoColaboradores.
     */
    @Test
    public void testRemoveColab() {
        System.out.println("removeColab");
        Colaborador colabAntigo = new Colaborador("1234", "Tiago Ribeiro");;
        RegistoColaboradores.removeColab(colabAntigo);
    }

    /**
     * Test of setRegistoColaboradores method, of class RegistoColaboradores.
     */
    @Ignore
    public void testSetRegistoColaboradores() {
        System.out.println("setRegistoColaboradores");
        ArrayList<Colaborador> registoColaboradores = null;
        RegistoColaboradores.setRegistoColaboradores(registoColaboradores);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
