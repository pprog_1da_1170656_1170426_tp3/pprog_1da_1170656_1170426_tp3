/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Registos;

import Model.Cartao;
import Model.CartaoDefinitivo;
import Model.CartaoProvisorio;
import java.util.ArrayList;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Ana Isabel Fonseca
 */
public class RegistoCartoesTest {
    
    public RegistoCartoesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of novoCartProvisorio method, of class RegistoCartoes.
     */
    @Test
    public void testNovoCartProvisorio() {
        System.out.println("novoCartProvisorio");
        String idCartao = "5678";
        String idColabAtribuido = "1234";
        String duracao = "10";
        RegistoCartoes.novoCartProvisorio(idCartao, idColabAtribuido, duracao);
    }

    /**
     * Test of novoCartDefinitivo method, of class RegistoCartoes.
     */
    @Test
    public void testNovoCartDefinitivo() {
        System.out.println("novoCartDefinitivo");
        String idCartao = "5678";
        String idColabAtribuido = "1234";
        RegistoCartoes.novoCartDefinitivo(idCartao, idColabAtribuido);
    }

    /**
     * Test of addProvisorio method, of class RegistoCartoes.
     */
    @Test
    public void testAddProvisorio() {
        System.out.println("addProvisorio");
        CartaoProvisorio cart = new CartaoProvisorio("5678", "1234" , "10");
        RegistoCartoes.addProvisorio(cart);
    }

    /**
     * Test of addDefinitivo method, of class RegistoCartoes.
     */
    @Test
    public void testAddDefinitivo() {
        System.out.println("addDefinitivo");
        CartaoDefinitivo cart = new CartaoDefinitivo("5678", "1234");;
        RegistoCartoes.addDefinitivo(cart);
    }

    /**
     * Test of validaProvisorio method, of class RegistoCartoes.
     */
    @Ignore
    public void testValidaProvisorio() {
        System.out.println("validaProvisorio");
        Cartao cart = null;
        boolean expResult = false;
        boolean result = RegistoCartoes.validaProvisorio(cart);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of validaDefinitivo method, of class RegistoCartoes.
     */
    @Ignore
    public void testValidaDefinitivo() {
        System.out.println("validaDefinitivo");
        Cartao cart = null;
        boolean expResult = false;
        boolean result = RegistoCartoes.validaDefinitivo(cart);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getCartaoDefinitivo method, of class RegistoCartoes.
     */
    @Ignore
    public void testGetCartaoDefinitivo() {
        System.out.println("getCartaoDefinitivo");
        String idCartao = "";
        Cartao expResult = null;
        Cartao result = RegistoCartoes.getCartaoDefinitivo(idCartao);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getCartaoProvisorio method, of class RegistoCartoes.
     */
    @Ignore
    public void testGetCartaoProvisorio() {
        System.out.println("getCartaoProvisorio");
        String idCartao = "";
        Cartao expResult = null;
        Cartao result = RegistoCartoes.getCartaoProvisorio(idCartao);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setRegistos method, of class RegistoCartoes.
     */
    @Ignore
    public void testSetRegistos() {
        System.out.println("setRegistos");
        ArrayList<CartaoProvisorio> registoCartoesP = null;
        ArrayList<CartaoDefinitivo> registoCartoesD = null;
        RegistoCartoes.setRegistos(registoCartoesP, registoCartoesD);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRegistoCartoesP method, of class RegistoCartoes.
     */
    @Ignore
    public void testGetRegistoCartoesP() {
        System.out.println("getRegistoCartoesP");
        ArrayList<CartaoProvisorio> expResult = null;
        ArrayList<CartaoProvisorio> result = RegistoCartoes.getRegistoCartoesP();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRegistoCartoesD method, of class RegistoCartoes.
     */
    @Ignore
    public void testGetRegistoCartoesD() {
        System.out.println("getRegistoCartoesD");
        ArrayList<CartaoDefinitivo> expResult = null;
        ArrayList<CartaoDefinitivo> result = RegistoCartoes.getRegistoCartoesD();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
