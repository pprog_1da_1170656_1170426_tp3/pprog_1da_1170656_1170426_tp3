package Controller;

import Registos.RegistoCartoes;
import Registos.RegistoColaboradores;
import Registos.RegistoEquipamentos;
import Utils.Data;
import Utils.FileManager;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

public class MainController {

    static Stage stgArea;
    static Stage stgPerfil;
    @FXML
    private Button btnPerfil;
    @FXML
    private Button btnArea;
    @FXML
    private Button btnFechar;
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private Label lblDataM;
    @FXML
    private Label lblDataI;
    @FXML
    private Button btnFileChooser;
    @FXML
    private TextField txtPathFicheiro;
    @FXML
    private Button btnImportar;
    @FXML
    private Button btnExportar;
    @FXML
    private Button btnFileChooserEX;
    @FXML
    private TextField txtFicheiroExportar;
    @FXML
    private Label lblDadosImportados;
    @FXML
    private Button btnImportarAnterior;

    public void mudarPerfil() {

    }

    @FXML
    /**
     * Método que controla o botão que permite ao utilizador aceder ao menu de
     * registo de perfil pela interface gráfica
     */
    private void handleButtonRegistarPerfil(javafx.event.ActionEvent event) throws IOException {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ScenePerfil.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            stgPerfil = new Stage();
            stgPerfil.setScene(new Scene(root));
            stgPerfil.show();
            MainClassFX.stg.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    /**
     * Método que controla o botão que permite ao utilizador aceder ao menu de
     * acesso a uma área restrita pela interface gráfica
     */
    private void handleButtonAcederArea(javafx.event.ActionEvent event) throws IOException {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/SceneAcederAreaRestrita.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            stgArea = new Stage();
            stgArea.setScene(new Scene(root));
            stgArea.show();
            MainClassFX.stg.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    /**
     * Método que controla o botão que fecha o programa, guardando os dados
     * atuais da sessão
     */
    private void handleButtonFechar(javafx.event.ActionEvent event) throws IOException {
        FileManager.guardar(RegistoEquipamentos.getListaEquipamentos(), RegistoColaboradores.getListaColab(), RegistoCartoes.getRegistoCartoesD(), RegistoCartoes.getRegistoCartoesP());
        Alert alerta = new Alert(Alert.AlertType.INFORMATION);
        alerta.setTitle("Sucesso!");
        alerta.setHeaderText("Sessão guardada");
        alerta.setContentText("Dados da sessão atual guardados com sucesso.");
        alerta.showAndWait();
        MainClassFX.stg.close();
    }

    /**
     * Método que controla a inicialização da interface gráfica
     */
    public void initialize() {
        lblDataM.setText(Data.dataAtual().toString());
        lblDataI.setText(Data.dataAtual().toString());

    }

    @FXML
    /**
     * Método que controla o botão que permite ao utilizador de escolher o
     * percurso do ficheiro a ser importado
     */
    private void handleFileChooser(javafx.event.ActionEvent event) {
        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Abrir ficheiro de texto ou binário");
        File file = fileChooser.showOpenDialog(stage);
        if (file.getAbsolutePath() != "") {
            txtPathFicheiro.setText(file.getAbsolutePath());
        }
    }

    @FXML
    /**
     * Método que controla a importação de um ficheiro binário ou de texto
     */
    private void handleBtnImportar(javafx.event.ActionEvent event) throws FileNotFoundException, IOException, ClassNotFoundException, Exception {
        Alert alerta = new Alert(Alert.AlertType.INFORMATION);
        alerta.setTitle("Erro!");
        if (txtPathFicheiro.getText().equals("")) {
            alerta.setHeaderText("Nenhum ficheiro selecionado.");
            alerta.setContentText("Por favor, antes de importar, selecione um ficheiro com o botão acima.");
            alerta.showAndWait();
        } else {
            String[] split = txtPathFicheiro.getText().split("\\.");
            alerta.setTitle("Sucesso!");
            if (split[split.length - 1].equals("txt")) {
                FileManager.lerFicheiroTXT(new File(txtPathFicheiro.getText()));
                alerta.setHeaderText("Ficheiro importado");
                alerta.setContentText("Ficheiro " + txtPathFicheiro.getText() + " importado com sucesso.");
                alerta.showAndWait();
                txtPathFicheiro.setText("");
                lblDadosImportados.setVisible(false);
            } else {
                FileManager.ler(new File(txtPathFicheiro.getText()));
                alerta.setHeaderText("Ficheiro importado");
                alerta.setContentText("Ficheiro " + txtPathFicheiro.getText() + " importado com sucesso.");
                alerta.showAndWait();
                txtPathFicheiro.setText("");
                lblDadosImportados.setVisible(false);
            }
        }
    }

    @FXML
    /**
     * Método que controla a exportação de um ficheiro binário
     */
    private void handleBtnExportar(ActionEvent event) throws IOException {
        FileManager.guardar(new File(txtFicheiroExportar.getText()), RegistoEquipamentos.getListaEquipamentos(), RegistoColaboradores.getListaColab(), RegistoCartoes.getRegistoCartoesD(), RegistoCartoes.getRegistoCartoesP());
        Alert alerta = new Alert(Alert.AlertType.INFORMATION);
        alerta.setTitle("Sucesso!");
        alerta.setHeaderText("Ficheiro exportado");
        alerta.setContentText("Ficheiro exportado com sucesso.");
        alerta.showAndWait();
        txtFicheiroExportar.setText("");
    }

    @FXML
    /**
     * Método que controla o botão que permite ao utilizador de escolher o
     * percurso do ficheiro a ser exportado
     */
    private void handleBtnChooserEX(ActionEvent event) {
        Stage window = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Exportar dados atuais.");
        fileChooser.getExtensionFilters().add(new ExtensionFilter("Ficheiro binário", ".bin"));
        File file = fileChooser.showSaveDialog(window);
        if (file != null) {
            txtFicheiroExportar.setText(file.getAbsolutePath());
        }
    }

    @FXML
    /**
     * Método que controla a importação de um ficheiro binário que contém os
     * dados da sessão anterior
     */
    private void handleBtnImportarAnterior(ActionEvent event) throws Exception {
        Alert alerta = new Alert(Alert.AlertType.INFORMATION);
        alerta.setTitle("Sucesso!");
        FileManager.ler();
        alerta.setHeaderText("Ficheiro importado");
        alerta.setContentText("Ficheiro " + txtPathFicheiro.getText() + " importado com sucesso.");
        alerta.showAndWait();
        txtPathFicheiro.setText("");
        lblDadosImportados.setVisible(false);
    }
}
