package Controller;

import java.io.IOException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


/**
 *
 * @author TiagoRibeiro(1170426
 */
public class MainClassFX extends Application {
    
    static Stage stg;
    
    @Override
    public void start(Stage stage) throws IOException {
        this.stg = stage;
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/Main.fxml"));
        
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");
        
        stg.setTitle("Programa PPROG e ESOFT");
        stg.setScene(scene);
        stg.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Application.launch(MainClassFX.class, args);
    }
    
}
