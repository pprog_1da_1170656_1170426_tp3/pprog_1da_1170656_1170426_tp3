package Controller;

import Model.Colaborador;
import Model.Equipamento;
import Model.Perfil;
import Model.PeriodoAutorizacao;
import Registos.ListaPeriodoAutorizacao;
import Registos.RegistoColaboradores;
import Registos.RegistoEquipamentos;
import Registos.RegistoPerfis;
import Utils.Data;
import Utils.Tempo;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

public class RegistarPerfilController {

    Perfil p;
    @FXML
    private Button btnRegistar;
    @FXML
    private Button btnCancelar;
    @FXML
    private TextField txtIDPerfil;
    @FXML
    private TextField txtDescricao;
    @FXML
    private ComboBox<String> cBoxListaEquipamentos;
    @FXML
    private ComboBox<String> cBoxListaDias;
    @FXML
    private TextField txtHorasInicio;
    @FXML
    private TextField txtMinInicio;
    @FXML
    private TextField txtHorasFim;
    @FXML
    private TextField txtMinFim;
    @FXML
    private Button btnPeriodo;
    @FXML
    private ListView<String> listPeriodos;
    private ListaPeriodoAutorizacao listaPA = new ListaPeriodoAutorizacao();
    private static final int MAX_HORAS = 24;
    private static final int MAX_MINUTOS = 60;
    private static final int MIN_TEMPO = 0;
    @FXML
    private ComboBox<String> cBoxListaColab;

    @FXML
    /**
     * Método que controla o botão que tenta guardar os dados e registar o
     * perfil em questão caso os dados estejam todos inseridos corretamente
     */
    private void handleButtonRegistar(ActionEvent event) {
        Alert alerta = new Alert(Alert.AlertType.INFORMATION);
        alerta.setTitle("Erro!");
        if (listaPA.getLista().isEmpty()) {
            alerta.setHeaderText("Nenhum periodo de autorização adicionado.");
            alerta.setContentText("Por favor, adicione pelo menos um periodo de autorização.");
            alerta.showAndWait();
        } else {
            if (txtIDPerfil.getText().trim().isEmpty() || txtDescricao.getText().trim().isEmpty() || cBoxListaColab.getValue() == null) {
                alerta.setHeaderText("ID e/ou descrição e/ou colaborador inválidos.");
                alerta.setContentText("Por favor, escreva um ID e/ou uma descrição e/ou colaborador válidos.");
                alerta.showAndWait();
            } else {
                boolean sucesso = RegistoPerfis.novoPerfil(txtIDPerfil.getText(), txtDescricao.getText(), listaPA, RegistoColaboradores.getColabByID(cBoxListaColab.getValue()));
                if (sucesso) {
                    alerta.setTitle("Sucesso");
                    alerta.setHeaderText("Perfil registado com sucesso.");
                    alerta.setContentText("Perfil e lista de periodos de autorização registados com sucesso.");
                    alerta.showAndWait();;
                    listaPA = new ListaPeriodoAutorizacao();
                    listPeriodos.getItems().clear();
                    txtIDPerfil.setText(null);
                    txtDescricao.setText(null);
                    txtHorasInicio.setText(null);
                    txtHorasFim.setText(null);
                    txtMinInicio.setText(null);
                    txtMinFim.setText(null);
                    cBoxListaDias.setValue(null);
                    cBoxListaEquipamentos.setValue(null);
                }
            }
        }
    }

    @FXML
    /**
     * Método que controla o botão que permite ao utilizador de voltar ao menu
     * inicial
     */
    private void handleButtonCancelar(ActionEvent event) throws IOException {
        MainClassFX.stg.show();
        MainController.stgPerfil.close();
    }

    /**
     * Método que controla a inicialização da interface gráfica
     */
    public void initialize() {
        for (Equipamento equipamento : RegistoEquipamentos.getListaEquipamentos()) {
            cBoxListaEquipamentos.getItems().addAll(equipamento.toString());
        }
        for (Colaborador colab : RegistoColaboradores.getListaColab()) {
            cBoxListaColab.getItems().addAll(colab.toString());
        }
        ArrayList<String> diasDaSemana = new ArrayList<>();
        for (String dia : Data.getDiasDaSemana()) {
            diasDaSemana.add(dia);
        }
        ObservableList obList = FXCollections.observableList(diasDaSemana);
        cBoxListaDias.setItems(obList);

    }

    @FXML
    /**
     * Método que controla o botão que adiciona um período de autorização caso
     * os dados deste estejam corretos
     */
    private void handleButtonPeriodo(ActionEvent event) {
        Alert alerta = new Alert(Alert.AlertType.INFORMATION);
        alerta.setTitle("Erro!");
        try {
            int hInicio = Integer.parseInt(txtHorasInicio.getText());
            int hFim = Integer.parseInt(txtHorasFim.getText());
            int minInicio = Integer.parseInt(txtMinInicio.getText());
            int minFim = Integer.parseInt(txtMinFim.getText());
            Tempo tempoInicio = new Tempo(hInicio, minInicio);
            Tempo tempoFim = new Tempo(hFim, minFim);
            if (hInicio > MAX_HORAS || hFim > MAX_HORAS || minInicio > MAX_MINUTOS || minFim > MAX_MINUTOS || hInicio < MIN_TEMPO || hFim < MIN_TEMPO || minInicio < MIN_TEMPO || minFim < MIN_TEMPO) {
                alerta.setHeaderText("Erro nos tempos inseridos.");
                alerta.setContentText("Por favor, insira um tempo válido.");
                alerta.showAndWait();
            } else if (cBoxListaDias.getValue() == null) {
                alerta.setHeaderText("Nenhum dia selecionado.");
                alerta.setContentText("Por favor, selecione um dia.");
                alerta.showAndWait();
            } else if (cBoxListaEquipamentos.getValue() == null) {
                alerta.setHeaderText("Nenhum equipamento selecionado.");
                alerta.setContentText("Por favor, selecione um equipamento.");
                alerta.showAndWait();
            } else if (tempoInicio.isMaior(tempoFim)) {
                alerta.setHeaderText("Erro nos tempos inseridos.");
                alerta.setContentText("Por favor, insira um intervalo de tempo válido.");
                alerta.showAndWait();
            } else {
                String diaSemana = cBoxListaDias.getValue();
                boolean sucesso = listaPA.criaPeriodo(RegistoEquipamentos.procurarEquipamento(cBoxListaEquipamentos.getValue()), diaSemana, tempoInicio, tempoFim);
                if (sucesso) {
                    listPeriodos.getItems().clear();
                    for (PeriodoAutorizacao pa : listaPA.getLista()) {
                        listPeriodos.getItems().addAll(pa.toString());
                    }
                } else {
                    alerta.setHeaderText("Periodo já existe!");
                    alerta.setContentText("O periodo inserido já existe no registo deste perfil.");
                    alerta.showAndWait();
                }

            }
        } catch (NumberFormatException e) {
            alerta.setHeaderText("Erro nos tempos inseridos.");
            alerta.setContentText("Por favor, insira um tempo válido.");
            alerta.showAndWait();
        }
    }
}
