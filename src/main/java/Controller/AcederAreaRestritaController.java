package Controller;

import Model.Equipamento;
import Registos.RegistoAcessos;
import Registos.RegistoCartoes;
import Registos.RegistoEquipamentos;
import Utils.Data;
import Utils.Tempo;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class AcederAreaRestritaController {

    @FXML
    private TextField txtIDCartao;
    @FXML
    private Button btnAceder;
    @FXML
    private Button btnCancelar;
    @FXML
    private ComboBox<String> cBoxListaEquipamentos;

    /**
     * Método que verifica se um certo colaborador tem acesso a um certo
     * equipamento
     *
     * @param idEquip = id do equipamento que controla a área que é se quer
     * aceder
     * @param idCartao = id do cartão do colaborador que pretend aceder
     * @param data = dia em que o acesso acontece (data atual)
     * @param hora = hora em que acesso acontece (hora atual)
     * @return tem acesso (true) ou não (false)
     */
    public boolean solicitarAcesso(String idEquip, String idCartao, Data data, Tempo hora) {
        if (RegistoCartoes.getCartaoDefinitivo(idCartao) != null) {
            return RegistoCartoes.getCartaoDefinitivo(idCartao).getColaboradorAtribuido(data).getPerfil().isEquipamentoAutorizado(RegistoEquipamentos.procurarEquipamento(idEquip), data, hora);
        }
        return RegistoCartoes.getCartaoProvisorio(idCartao).getColaboradorAtribuido(data).getPerfil().isEquipamentoAutorizado(RegistoEquipamentos.procurarEquipamento(idEquip), data, hora);
    }

    @FXML
    /**
     * Método que controla o botão que permite ao utilizador de voltar ao menu
     * inicial
     */
    private void cancelarAcederArea(ActionEvent event) throws IOException {
        MainClassFX.stg.show();
        MainController.stgArea.close();
    }

    /**
     * Método que controla a inicialização da interface gráfica
     */
    public void initialize() {
        for (Equipamento equipamento : RegistoEquipamentos.getListaEquipamentos()) {
            cBoxListaEquipamentos.getItems().addAll(equipamento.toString());
        }
    }

    @FXML
    /**
     * Método que controla o botão que permite ao utilizador tentar aceder à
     * area em questão
     */
    private void verificarAceder(ActionEvent event) {
        Alert alerta = new Alert(AlertType.INFORMATION);
        alerta.setTitle("Erro!");
        if (cBoxListaEquipamentos.getValue() == null) {
            alerta.setHeaderText("Nenhuma área selecionada!");
            alerta.setContentText("Não selecionou nenhuma área, por favor selecione uma e volte a tentar.");
            alerta.showAndWait();
        } else {
            if (txtIDCartao.getText().trim().isEmpty()) {
                alerta.setHeaderText("Nenhum ID de cartão inserido!");
                alerta.setContentText("Não inseriu nenhum ID de cartão, por favor insira um e volte a tentar.");
                alerta.showAndWait();
            } else {
                if (RegistoCartoes.getCartaoDefinitivo(txtIDCartao.getText()) == null && RegistoCartoes.getCartaoDefinitivo(txtIDCartao.getText()) == null) {
                    alerta.setHeaderText("ID de cartão inexistente");
                    alerta.setContentText("O ID de cartão inserido não corresponde a nenhum cartão conhecido, por favor insira outro ID e tente novamente.");
                    alerta.showAndWait();
                } else if (RegistoCartoes.getCartaoDefinitivo(txtIDCartao.getText()) != null) {
                    boolean acessoSolicitado = solicitarAcesso(cBoxListaEquipamentos.getValue(), txtIDCartao.getText(), Data.dataAtual(), Tempo.tempoAtual());
                    RegistoAcessos.novoAcesso(RegistoEquipamentos.procurarEquipamento(cBoxListaEquipamentos.getValue()), RegistoCartoes.getCartaoDefinitivo(txtIDCartao.getText()), RegistoCartoes.getCartaoDefinitivo(txtIDCartao.getText()).getColaboradorAtribuido(Data.dataAtual()), Data.dataAtual(), Tempo.tempoAtual(), acessoSolicitado, "entrada");
                    if (acessoSolicitado) {
                        alerta.setTitle("Acesso permitido");
                        alerta.setHeaderText("Acesso à área gerida pelo equipamento " + RegistoEquipamentos.procurarEquipamento(cBoxListaEquipamentos.getValue()).toString() + " permitido");
                        alerta.setContentText("Acesso à área gerida pelo equipamento " + RegistoEquipamentos.procurarEquipamento(cBoxListaEquipamentos.getValue()).toString() + " foi permitido ao colaborador " + RegistoCartoes.getCartaoDefinitivo(txtIDCartao.getText()).getColaboradorAtribuido(Data.dataAtual()).toString());
                        alerta.showAndWait();
                        txtIDCartao.setText(null);
                        cBoxListaEquipamentos.setValue(null);
                    } else {
                        alerta.setTitle("Acesso negado");
                        alerta.setHeaderText("Acesso à área gerida pelo equipamento " + RegistoEquipamentos.procurarEquipamento(cBoxListaEquipamentos.getValue()).toString() + " negado");
                        alerta.setContentText("Acesso à área gerida pelo equipamento " + RegistoEquipamentos.procurarEquipamento(cBoxListaEquipamentos.getValue()).toString() + " foi negado ao colaborador " + RegistoCartoes.getCartaoDefinitivo(txtIDCartao.getText()).getColaboradorAtribuido(Data.dataAtual()).toString());
                        alerta.showAndWait();
                        txtIDCartao.setText(null);
                        cBoxListaEquipamentos.setValue(null);
                    }
                } else {
                    boolean acessoSolicitado = solicitarAcesso(cBoxListaEquipamentos.getValue(), txtIDCartao.getText(), Data.dataAtual(), Tempo.tempoAtual());
                    RegistoAcessos.novoAcesso(RegistoEquipamentos.procurarEquipamento(cBoxListaEquipamentos.getValue()), RegistoCartoes.getCartaoProvisorio(txtIDCartao.getText()), RegistoCartoes.getCartaoDefinitivo(txtIDCartao.getText()).getColaboradorAtribuido(Data.dataAtual()), Data.dataAtual(), Tempo.tempoAtual(), acessoSolicitado, "entrada");
                    if (acessoSolicitado) {
                        alerta.setTitle("Acesso permitido");
                        alerta.setHeaderText("Acesso à área gerida pelo equipamento " + cBoxListaEquipamentos.getValue() + " permitido");
                        alerta.setContentText("Acesso à área gerida pelo equipamento " + cBoxListaEquipamentos.getValue() + " foi permitido ao colaborador " + RegistoCartoes.getCartaoProvisorio(txtIDCartao.getText()).getColaboradorAtribuido(Data.dataAtual()).toString());
                        alerta.showAndWait();
                        txtIDCartao.setText(null);
                        cBoxListaEquipamentos.setValue(null);
                    } else {
                        alerta.setTitle("Acesso negado");
                        alerta.setHeaderText("Acesso à área gerida pelo equipamento " + cBoxListaEquipamentos.getValue() + " negado");
                        alerta.setContentText("Acesso à área gerida pelo equipamento " + cBoxListaEquipamentos.getValue() + " foi negado ao colaborador " + RegistoCartoes.getCartaoProvisorio(txtIDCartao.getText()).getColaboradorAtribuido(Data.dataAtual()).toString());
                        alerta.showAndWait();
                        txtIDCartao.setText(null);
                        cBoxListaEquipamentos.setValue(null);
                    }
                }
            }

        }
    }

}
