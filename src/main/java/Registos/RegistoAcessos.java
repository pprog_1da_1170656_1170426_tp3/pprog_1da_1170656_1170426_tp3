package Registos;

import Model.Acesso;
import Model.Cartao;
import Model.Colaborador;
import Model.Equipamento;
import Utils.Data;
import Utils.Tempo;
import java.util.ArrayList;

public class RegistoAcessos {

    public static ArrayList<Acesso> registoAcessos = new ArrayList<>();

    /**
     * Regista uma tentativa de acesso a uma área por um equipamento no registo
     * de acessos com o dia e a hora em que aconteceu seguido do sucesso
     * (permitido ou negado)
     *
     * @param eq = equipamento em questão
     * @param cart = cartão do utilizador que tentou aceder
     * @param colab = colaborador que tentou aceder
     * @param data = data da tentativa de acesso
     * @param hora = hora da tentativa de acesso
     * @param sucesso = sucesso (true ou false)
     * @param mov = entrada
     */
    public static void novoAcesso(Equipamento eq, Cartao cart, Colaborador colab, Data data, Tempo hora, boolean sucesso, String mov) {
        Acesso a = new Acesso(data, hora, mov, sucesso, eq.toString(), colab.toString());
        addAcesso(a);
    }

    /**
     * Adiciona a tentativa de acesso ao registo de acessos
     *
     * @param acesso = tentativa de acesso
     */
    public static void addAcesso(Acesso acesso) {
        registoAcessos.add(acesso);
    }
}
