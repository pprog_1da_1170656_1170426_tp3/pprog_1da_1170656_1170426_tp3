package Registos;

import Model.Equipamento;
import Model.PeriodoAutorizacao;
import Utils.Tempo;
import java.util.ArrayList;

public class ListaPeriodoAutorizacao {

    private ArrayList<PeriodoAutorizacao> registoPA = new ArrayList<>();

    /**
     * Método que cria um periodo de autorização se já não existir um igual
     *
     * @param e = equipamento a ser associado ao periodo
     * @param diaSemana = dia da semana em que o periodo é aplicável
     * @param tempoInicio = hora inicial em que o periodo é aplicável
     * @param tempoFim = hora final em que o período é aplicável
     * @return true caso tenha sido criado com sucesso, false caso contrário
     */
    public boolean criaPeriodo(Equipamento e, String diaSemana, Tempo tempoInicio, Tempo tempoFim) {
        PeriodoAutorizacao pa = new PeriodoAutorizacao(e, diaSemana, tempoInicio, tempoFim);
        if (!valida(pa)) {
            return false;
        }
        addPeriodo(pa);
        return true;
    }

    /**
     * Adiciona o periodo ao registo
     * @param pa = periodo a ser adicionado
     */
    public void addPeriodo(PeriodoAutorizacao pa) {
        registoPA.add(pa);
    }

    /**
     * Método que verifica se já existe algum periodo de autorização similar
     * @param pa = periodo a verificar
     * @return true se não existir nenhum, false caso contrário
     */
    public boolean valida(PeriodoAutorizacao pa) {
        for (PeriodoAutorizacao pa1 : registoPA) {
            if (pa1.equals(pa)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Método que devolve a lista de períodos de autorização
     * @return a lista de períodos de autorização
     */
    public ArrayList<PeriodoAutorizacao> getLista() {
        return registoPA;
    }
}
