package Registos;

import Model.Equipamento;
import java.io.File;
import java.util.ArrayList;

public class RegistoEquipamentos {

    private static ArrayList<Equipamento> registoEquipamentos = new ArrayList<>();

    /**
     * Devolve o registo de equipamentos
     *
     * @return
     */
    public static ArrayList<Equipamento> getListaEquipamentos() {
        return registoEquipamentos;
    }

    /**
     * Cria um novo equipamento e adiciona-o ao registo caso seja valido
     *
     * @param endLogico = endereço lógico (id)
     * @param endFisico = endereço físico
     * @param descricao = descrição
     * @param fxConf = ficheiro de configuração
     * @param movimento = entrada ou saída
     */
    public static void novoEquipamento(String endLogico, String endFisico, String descricao, File fxConf, String movimento) {
        Equipamento eq = new Equipamento(endLogico, endFisico, descricao, fxConf, movimento);
        if (valida(eq)) {
            add(eq);
        }
    }

    /**
     * Devolve um equipamento conhecendo o seu id
     *
     * @param idEquipamento = id do equipamento
     * @return indice do equipamento corresponde ao id se existir, senão null
     */
    public static Equipamento procurarEquipamento(String idEquipamento) {
        for (int j = 0; j < registoEquipamentos.size(); j++) {
            if (idEquipamento.compareToIgnoreCase(registoEquipamentos.get(j).toString()) == 0) {
                return registoEquipamentos.get(j);
            }
        }
        return null;
    }

    /**
     * Verifica se já existe um certo equipamento no registo
     *
     * @param equip = equipamento a validar
     * @return true se não existir, false se existir
     */
    public static boolean valida(Equipamento equip) {
        for (Equipamento equip1 : registoEquipamentos) {
            if (equip1.equals(equip)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Adiciona um equipamento ao registo
     *
     * @param e = equipamento
     */
    public static void add(Equipamento e) {
        registoEquipamentos.add(e);
    }
    
    /**
     * Recebe um registo e torna-o o atual
     * @param registoEquipamentos 
     */
    public static void setRegistoEquipamentos(ArrayList<Equipamento> registoEquipamentos) {
        for (Equipamento eq : registoEquipamentos) {
            add(eq);
        }
    }
}
