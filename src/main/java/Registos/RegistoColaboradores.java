package Registos;

import Model.Colaborador;
import java.util.ArrayList;

public class RegistoColaboradores {

    private static ArrayList<Colaborador> registoColab = new ArrayList<>();

    /**
     * Cria um novo colaborador e adiciona-o ao registo caso seja valido
     *
     * @param numMecanografico = número mecanográfico do colaborador
     * @param nomeCompleto = nome completo do colaborador
     */
    public static void novoColaborador(String numMecanografico, String nomeCompleto) {
        Colaborador colab = new Colaborador(numMecanografico, nomeCompleto);
        if (valida(colab)) {
            add(colab);
        }
    }

    /**
     * Adiciona um colaborador ao registo
     *
     * @param colab = colaborador
     */
    public static void add(Colaborador colab) {
        registoColab.add(colab);
    }

    /**
     * Verifica se já existe um certo colaborador no registo
     *
     * @param colab = colaborador a validar
     * @return true se não existir, false se existir
     */
    public static boolean valida(Colaborador colab) {
        for (Colaborador colab1 : registoColab) {
            if (colab1.equals(colab)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Devolve um colaborador encontrando-o pelo seu ID (número mecanográfico)
     *
     * @param colabID = número mecanográfico do colaborador
     * @return colaborador correspondente ao id
     */
    public static Colaborador getColabByID(String colabID) {
        for (Colaborador colab : registoColab) {
            if (colab.toString().equals(colabID)) {
                return colab;
            }
        }
        return null;
    }

    /**
     * Devolve o registo de colaboradores
     *
     * @return registo de colaboradores
     */
    public static ArrayList<Colaborador> getListaColab() {
        return registoColab;
    }

    /**
     * Remove um colaborador do registo
     *
     * @param colabAntigo = colaborador a remover
     */
    public static void removeColab(Colaborador colabAntigo) {
        registoColab.remove(colabAntigo);
    }

    /**
     * Recebe um registo e torna-o o atual
     * @param registoColaboradores 
     */
    public static void setRegistoColaboradores(ArrayList<Colaborador> registoColaboradores) {
        for (Colaborador colab : registoColaboradores) {
            add(colab);
        }
    }
}
