package Registos;

import Model.Colaborador;
import Model.Perfil;
import java.util.ArrayList;

public class RegistoPerfis {

    /**
     * ArrayList dos Vários Perfis criados.
     */
    private static ArrayList<Perfil> registoPerfis = new ArrayList<>();

    /**
     * Construtor de um novo perfil.
     *
     * @param idPerfil ID do perfil
     * @param descricao Descrição do Perfil.
     * @param listaPA Lista de Períodos de Autorização.
     * @param colab Colaborador associado.
     * @return retorna true se este perfil for criado.
     */
    public static boolean novoPerfil(String idPerfil, String descricao, ListaPeriodoAutorizacao listaPA, Colaborador colab) {
        Perfil p = new Perfil(idPerfil, descricao, listaPA);
        if (!valida(p)) {
            return false;
        }
        if (RegistoColaboradores.getColabByID(colab.toString()) == null) {
            addPerfil(p);
            colab.setPerfil(p);
            RegistoColaboradores.add(colab);
        } else {
            RegistoColaboradores.removeColab(colab);
            addPerfil(p);
            colab.setPerfil(p);
            RegistoColaboradores.add(colab);
        }

        return true;
    }

    /**
     * Validação do perfil, verifica se o perfil de autorização já existe.
     *
     * @param p perfil a verificar
     * @return retorna boolean true se não existe.
     */
    public static boolean valida(Perfil p) {
        for (Perfil p1 : registoPerfis) {
            if (p1.equals(p)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Método que adiciona perfil à ArrayList.
     *
     * @param p = Perfil
     */
    public static void addPerfil(Perfil p) {
        registoPerfis.add(p);
    }
}
