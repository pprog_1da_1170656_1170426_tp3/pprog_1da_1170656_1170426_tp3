package Registos;

import Model.Cartao;
import Model.CartaoDefinitivo;
import Model.CartaoProvisorio;
import java.util.ArrayList;

public class RegistoCartoes {

    private static ArrayList<CartaoProvisorio> registoCartoesP = new ArrayList<>();
    private static ArrayList<CartaoDefinitivo> registoCartoesD = new ArrayList<>();

    /**
     * Cria um novo cartão provisório e adiciona-o ao registo caso seja valido
     *
     * @param idCartao = id do cartão a ser criado
     * @param idColabAtribuido = id do colaborador a ser atribuido ao cartão
     * @param duracao = duração do cartão em horas
     */
    public static void novoCartProvisorio(String idCartao, String idColabAtribuido, String duracao) {
        CartaoProvisorio cartP = new CartaoProvisorio(idCartao, idColabAtribuido, duracao);
        if (validaProvisorio(cartP)) {
            addProvisorio(cartP);
        }
    }

    /**
     * /**
     * Cria um novo cartão definitivo e adiciona-o ao registo caso seja valido
     *
     * @param idCartao = id do cartão a ser criado
     * @param idColabAtribuido = id do colaborador a ser atribuido ao cartão
     */
    public static void novoCartDefinitivo(String idCartao, String idColabAtribuido) {
        CartaoDefinitivo cartD = new CartaoDefinitivo(idCartao, idColabAtribuido);
        if (validaDefinitivo(cartD)) {
            addDefinitivo(cartD);
        }
    }

    /**
     * Adiciona um cartão provisório ao registo
     *
     * @param cart = cartão provisório
     */
    public static void addProvisorio(CartaoProvisorio cart) {
        registoCartoesP.add(cart);
    }

    /**
     * Adiciona um cartão definitivo ao registo
     *
     * @param cart = cartão definitivo
     */
    public static void addDefinitivo(CartaoDefinitivo cart) {
        registoCartoesD.add(cart);
    }

    /**
     * Verifica se um cartão provisório já existe no registo
     *
     * @param cart = cartão a validar
     * @return true se for válido, false em caso contrário
     */
    public static boolean validaProvisorio(Cartao cart) {
        for (CartaoProvisorio cartao : registoCartoesP) {
            if (cartao.getIDCartao().equals(cart.getIDCartao())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Verifica se um cartão definitivo já existe no registo
     *
     * @param cart = cartão a validar
     * @return true se for válido, false em caso contrário
     */
    public static boolean validaDefinitivo(Cartao cart) {
        for (CartaoDefinitivo cartao : registoCartoesD) {
            if (cartao.getIDCartao().equals(cart.getIDCartao())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Devolve um cartão definitivo a partir do seu ID
     *
     * @param idCartao = id do cartão
     * @return cartão que corresponde ao id
     */
    public static Cartao getCartaoDefinitivo(String idCartao) {
        for (CartaoDefinitivo cartao : registoCartoesD) {
            if (cartao.getIDCartao().equals(idCartao)) {
                return cartao;
            }
        }
        return null;
    }

    /**
     * Devolve um cartão definitivo a partir do seu ID
     *
     * @param idCartao = id do cartão
     * @return cartão que corresponde ao id
     */
    public static Cartao getCartaoProvisorio(String idCartao) {
        for (CartaoProvisorio cartao : registoCartoesP) {
            if (cartao.getIDCartao().equals(idCartao)) {
                return cartao;
            }
        }
        return null;
    }

    /**
     * Recebendo os registos torna-os os atuais
     * @param registoCartoesP = registo de cartões provisórios
     * @param registoCartoesD = registo de cartões definitivos
     */
    public static void setRegistos(ArrayList<CartaoProvisorio> registoCartoesP, ArrayList<CartaoDefinitivo> registoCartoesD) {
        for (CartaoProvisorio cartaoP : registoCartoesP) {
            addProvisorio(cartaoP);
        }
        for (CartaoDefinitivo cartaoD : registoCartoesD) {
            addDefinitivo(cartaoD);
        }
    }

    /**
     * Devolve o registo de cartões provisórios
     *
     * @return registo de cartões provisórios
     */
    public static ArrayList<CartaoProvisorio> getRegistoCartoesP() {
        return registoCartoesP;
    }

    /**
     * Devolve o registo de cartões definitivos
     *
     * @return registo de cartões definitivos
     */
    public static ArrayList<CartaoDefinitivo> getRegistoCartoesD() {
        return registoCartoesD;
    }

}
