package Model;

import Registos.ListaPeriodoAutorizacao;
import Utils.Data;
import Utils.Tempo;

public class Perfil {

    /**
     * Id do Perfil de Autorização.
     */
    private String id;

    /**
     * Descrição do Perfil de Autorização.
     */
    private String descricao;

    /**
     * Lista de Períodos de Autorização.
     */
    private ListaPeriodoAutorizacao listaPA;

    /**
     * Construtor do perfil de autorização.
     *
     * @param id Id do Perfil
     * @param descricao Descrição do Perfil
     * @param listaPA lista de períodos de autorização.
     */
    public Perfil(String id, String descricao, ListaPeriodoAutorizacao listaPA) {
        this.id = id;
        this.descricao = descricao;
        this.listaPA = listaPA;
    }

    /**
     * Método de Verificação de período de Autorização.
     *
     * @param eq Equipamento que vai ser verificado.
     * @param data Data que será testada.
     * @param hora Hora característica do período de tempo que será verificada.
     * @return Retorna um boolean
     */
    public boolean isEquipamentoAutorizado(Equipamento eq, Data data, Tempo hora) {
        String diaSemana = data.diaDaSemana();
        for (PeriodoAutorizacao pa : listaPA.getLista()) {
            if (pa.getEquipamento().toString().equals(eq.toString())) {
                if (diaSemana.equals(pa.getDiaSemana())) {
                    if (hora.isMaior(pa.getHoraInicio()) && !hora.isMaior(pa.getHoraFim())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Verificação de existência de um perfil.
     *
     * @param outroPerfil Perfil criado.
     * @return Retorna false se houver um perfil com as mesmas características e
     * retorna true se não houver.
     */
    public boolean equals(Perfil outroPerfil) {
        if (outroPerfil.getId().equals(id) && outroPerfil.getDescricao().equals(descricao) && outroPerfil.getListaPA().equals(listaPA)) {
            return true;
        }
        return false;
    }

    /**
     * Retorna o ID do Perfil.
     *
     * @return id perfil
     */
    public String getId() {
        return id;
    }

    /**
     * Retorna a Descrição do Perfil.
     *
     * @return Descrição do Perfil
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Retorna a Lista de Periodos de Autorização.
     *
     * @return lista dos períodos de autorização deste perfil
     */
    public ListaPeriodoAutorizacao getListaPA() {
        return listaPA;
    }

}
