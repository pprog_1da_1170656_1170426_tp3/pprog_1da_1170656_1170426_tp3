package Model;

import Registos.RegistoColaboradores;
import Utils.Data;

public class CartaoProvisorio implements Cartao {

    private String idCartao;

    private String idColaborador;

    private String duracao;

    /**
     * Construtor do cartão provisório
     *
     * @param idCartao = id do cartão
     * @param idColaborador = id do colaborador
     * @param duracao = duração do cartão em dias
     */
    public CartaoProvisorio(String idCartao, String idColaborador, String duracao) {
        this.idCartao = idCartao;
        this.idColaborador = idColaborador;
        this.duracao = duracao;
    }

    @Override
    /**
     * Método que permite obter o colaborador atribuido a um cartão
     *
     * @param data = data atual
     * @return o colaborador atribuido ao cartão em questão
     */
    public Colaborador getColaboradorAtribuido(Data data) {
        return RegistoColaboradores.getColabByID(idColaborador);
    }

    @Override
    /**
     * Método que devolve o id do Cartão em questão
     *
     * @return id do Cartão
     */
    public String getIDCartao() {
        return idCartao;
    }

}
