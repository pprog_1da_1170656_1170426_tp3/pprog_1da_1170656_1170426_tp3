package Model;

import java.io.Serializable;

public class Colaborador implements Serializable {

    /**
     * Número mecanográfico do Colaborador.
     */
    private String numMecanografico;

    /**
     * Nome Completo do Colaborador.
     */
    private String nomeCompleto;

    /**
     * Nome Abreviado do Colaborador.
     */
    private String nomeAbreviado;

    /**
     * Perfil do Colaborador.
     */
    private Perfil perfilAtribuido;
    
    /**
     * Número Mecanográfico por Omissão
     */
    private static final String NUMMEC_POR_OMISSAO = "00000000";

    /**
     * Nome Completo por omissão;
     */
    private static final String NOME_POR_OMISSAO = "Por definir ";

    /**
     * Nome Abreviado por omissão.
     */
    private static final String NOMEABREV_POR_OMISSAO = "PD";

    /**
     * Diferencia Colaboradores sem parâmetros.
     */
    private int i = 0;

    /**
     * Construtor com Parâmetros do Colaborador.
     *
     * @param numMecanografico Número Mecanográfico do utilizador.
     * @param nomeCompleto Nome Completo do Colaborador.
     */
    public Colaborador(String numMecanografico, String nomeCompleto) {
        this.numMecanografico = numMecanografico;
        this.nomeCompleto = nomeCompleto;
        this.nomeAbreviado = nomeAbreviado();
    }

    /**
     * Construtor sem parâmetros.
     */
    public Colaborador() {
        this.numMecanografico = NUMMEC_POR_OMISSAO;
        this.nomeCompleto = NOME_POR_OMISSAO + i;
        this.nomeAbreviado = NOMEABREV_POR_OMISSAO + i;
        i++;
    }

    public Perfil getPerfil() {
        return perfilAtribuido;
    }

    /**
     * Método que cria o nome abreviado apartir do completo.
     *
     * @return Nome Abreviado.
     */
    public String nomeAbreviado() {
        String[] split = this.nomeCompleto.trim().replaceAll("( )+", " ").split(" ");
        String nomeFinal = split[0] + " " + split[split.length - 1];
        return nomeFinal;
    }

    /**
     * Método que associa um perfil a este colaborador
     * @param p = perfil a ser associado
     */
    public void setPerfil(Perfil p){
        this.perfilAtribuido = p;
    }
    
    /**
     * toString que retorna apenas o número mecanográfico do Colaborador.
     *
     * @return número mecanográfico
     */
    public String toString() {
        return numMecanografico;
    }
    
}
