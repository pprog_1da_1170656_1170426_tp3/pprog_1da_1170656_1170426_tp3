package Model;

import Utils.Data;
import java.io.Serializable;

public interface Cartao extends Serializable{
    
    /**
     * Método que permite obter o colaborador atribuido a um cartão
     * @param data = data atual
     * @return o colaborador atribuido ao cartão em questão
     */
    public Colaborador getColaboradorAtribuido (Data data);
    
    /**
     * Método que devolve o id do Cartão em questão
     * @return id do Cartão
     */
    public String getIDCartao();
    
    
}
