package Model;

import Registos.RegistoCartoes;
import Registos.RegistoColaboradores;
import Registos.RegistoEquipamentos;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

public class Empresa {

    /**
     * Método que instancia os registos a serem guardados ao importar um
     * ficheiro de texto
     *
     * @param linha = linha de texto que contém as informações que permitem
     * instanciar os registos
     * @throws FileNotFoundException
     */
    public static void instanciarRegistos(String[] linha) throws FileNotFoundException {
        if (linha[0].trim().equals("Equip")) {
            RegistoEquipamentos.novoEquipamento(linha[1].trim(), linha[2].trim(), linha[3].trim(), new File(linha[4].trim()), linha[5].trim());
        } else {
            if (linha[0].trim().equals("Colab")) {
                RegistoColaboradores.novoColaborador(linha[1].trim(), linha[2].trim());
            } else {
                if (linha[0].trim().equals("CartD")) {
                    RegistoCartoes.novoCartDefinitivo(linha[1].trim(), linha[2].trim());
                } else {
                    if (linha[0].trim().equals("CartP")) {
                        RegistoCartoes.novoCartProvisorio(linha[1].trim(), linha[2].trim(), linha[3].trim());
                    }
                }
            }
        }
    }

    /**
     * Método que guarda os registos recebidos
     * @param registoColab = registo de colaboradores
     * @param registoEquip = registo de equipamentos
     * @param registoCartoesP = registo de cartões provisórios
     * @param registoCartoesD = registo de cartões definitivos
     */
    public static void setRegistos(ArrayList<Colaborador> registoColab, ArrayList<Equipamento> registoEquip, ArrayList<CartaoProvisorio> registoCartoesP, ArrayList<CartaoDefinitivo> registoCartoesD) {
        RegistoColaboradores.setRegistoColaboradores(registoColab);
        RegistoEquipamentos.setRegistoEquipamentos(registoEquip);
        RegistoCartoes.setRegistos(registoCartoesP, registoCartoesD);
    }
}
