package Model;

import java.io.File;
import java.io.Serializable;

public class Equipamento implements Serializable {

    /**
     * Endereço Lógico do Equipamento.
     */
    private String endLogico;

    /**
     * Endereço Físico do Equipamento.
     */
    private String endFisico;

    /**
     * Descrição do Equipamento.
     */
    private String descricao;

    /**
     * Ficheiro de Configuração do Equipamento.
     */
    private File fxConf;

    /**
     * Movimento associado ao Equipamento.
     */
    private String movimento;

    /**
     * Construtor com todos os parâmetros do Equipamento.
     *
     * @param endLogico Endereço Lógico do Equipamento
     * @param endFisico Endereço Físico do Equipamento.
     * @param descricao Descrição do Equipamento.
     * @param fxConf Ficheiro de Configuração do Equipamento.
     * @param movimento Movimento associado ao Equipamento.
     */
    public Equipamento(String endLogico, String endFisico, String descricao, File fxConf, String movimento) {
        this.endLogico = endLogico;
        this.endFisico = endFisico;
        this.descricao = descricao;
        this.fxConf = fxConf;
        this.movimento = movimento;
    }

    /**
     * Retorna o movimento associado ao equipamento
     *
     * @return Retorna o movimento associado.
     */
    public String getMovimento() {
        return movimento;
    }

    /**
     * Retorna apenas o endereço lógico associado ao equipamento.
     *
     * @return endereço lógico
     */
    public String toString() {
        return endLogico;
    }

}
