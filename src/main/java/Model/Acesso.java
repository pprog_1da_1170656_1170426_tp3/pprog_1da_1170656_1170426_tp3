package Model;

import Utils.Data;
import Utils.Tempo;

public class Acesso {
    /*
    Data de Acesso.
    */
    private Data data;
    
    /*
    Hora de Acesso.
    */
    private Tempo hora;
    
    /*
    Movimento (entrada ou saída) do Acesso
    */
    private String mov;
    
    /*
    Sucesso ou insucesso da tentativa de Acesso.
    */
    private boolean sucesso;
    
    /*
    Id do Equipamento para o qual ocorreu a tentativa de acesso.
    */
    private String idEquip;
    
    /*
    Id do Colaborador que tentou aceder.
    */
    private String idColab;
    
    /*
    Construtor da Classe acesso com todos os parâmetros.
    */
    public Acesso(Data data, Tempo hora, String mov, boolean sucesso, String idEquip, String idColab){
        this.data = data;
        this.hora = hora;
        this.mov = mov;
        this.sucesso = sucesso;
        this.idColab = idColab;
        this.idEquip = idEquip;
    }
    
}
