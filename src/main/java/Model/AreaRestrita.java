package Model;

import javafx.scene.control.Alert;

public class AreaRestrita {

    /**
     * Lotação Total da área Restrita.
     */
    private int lotacao = 100;
    
    /*
    Lotação de Referência utilizada nos testes de permissão de acesso.
    */
    private static int lotacaoAtual = 0;
    
    /**
     * Nome da Área Restrita
     */
    private String nome;
    
    /**
     * Nome por Omissão da Área Restrita
     */
    private final String NOME_POR_OMISSAO = "x";
    
    /**
     * Lotação por omissão da Área Restrita.
     */
    private final int LOTACAO_POR_OMISSAO = 0;
    
    /*
    Inteiro que irá diferenciar as diferentes áreas sem parâmetros.
    */
    int i = 0;
    
    /**
     * Movimento de Entrada.
     */
    private static final String MOVE = "entrada";

    
    /**
     * Construtor de Área Restrita com todos os parâmetros.
     * @param nome Nome da Área
     * @param lotacao Lotação da Área Restrita
     */
    public AreaRestrita(String nome, int lotacao) {
        this.nome = nome;
        this.lotacao = lotacao;
    }

    /**
     * Construtor com alerta de Área Restrita.
     */
    public AreaRestrita() {
        this.nome = NOME_POR_OMISSAO + i;
        this.lotacao = LOTACAO_POR_OMISSAO;
        i++;
        Alert alerta = new Alert(Alert.AlertType.INFORMATION);
        alerta.setTitle("Erro!");
        alerta.setHeaderText("Nenhuma característica foi inserida.");
        alerta.setContentText("Cada vez que cria uma Área sem qualquer característica será impossibilitada a entrada de qualquer indivíduo.");
        alerta.showAndWait();
    }

    /**
     * Verificação da Lotação para permitir o acesso ao colaborador.
     * @param mov Movimento pretendido
     * @return Autorização com sucesso ou sem.
     */
    public boolean lotacaoPermiteAcesso(String mov) {
        if (!mov.trim().equals(MOVE)) {
            lotacaoAtual--;
            return true;
        } else {
            if (lotacaoAtual < lotacao) {
                lotacaoAtual++;
                return true;
            } else {
                return false;
            }
        }
        
    }
    
    /**
     * Retorna o nome da Área Restrita.
     * @return nome da área.
     */
    public String toString() {
        return nome;
    }
}
