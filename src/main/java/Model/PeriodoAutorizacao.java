package Model;

import Utils.Tempo;

public class PeriodoAutorizacao {

    /**
     * Dia da Semana do Período de Autorização.
     */
    private String diaSemana;

    /**
     * Hora de Início do Período de Autorização.
     */
    private Tempo hora_Inicio;

    /**
     * Hora de Fim do Período de Autorização.
     */
    private Tempo hora_Fim;

    /**
     * Equipamento associado ao Período de Autorização.
     */
    private Equipamento equipamento;

    /**
     * Construtor de Período de Autorização com todos os parâmetros.
     *
     * @param equipamento Equipamento associado ao Período de Autorização.
     * @param diaSemana Dia da Semana do Período de Autorização.
     * @param hora_Inicio Hora de Início do Período de Autorização.
     * @param hora_Fim Hora de Fim do Período de Autorização.
     */
    public PeriodoAutorizacao(Equipamento equipamento, String diaSemana, Tempo hora_Inicio, Tempo hora_Fim) {
        this.equipamento = equipamento;
        this.diaSemana = diaSemana;
        this.hora_Inicio = hora_Inicio;
        this.hora_Fim = hora_Fim;
    }

    /**
     * Retorna o Equipamento associado ao PA.
     *
     * @return Equipamento associado ao PA.
     */
    public Equipamento getEquipamento() {
        return equipamento;
    }

    /**
     * Retorna a Hora de Inicio associada ao PA.
     *
     * @return Hora de Inicio associada ao PA.
     */
    public Tempo getHoraInicio() {
        return hora_Inicio;
    }

    /**
     * Retorna a Hora de Fim associada ao PA.
     *
     * @return Hora de Fim associada
     */
    public Tempo getHoraFim() {
        return hora_Fim;
    }

    /**
     * Retorna Dia da Semana do PA
     *
     * @return Retorna dia da semana do PA.
     */
    public String getDiaSemana() {
        return diaSemana;
    }

    /**
     * Verificação de Períodos de Autorização.
     *
     * @param outroPA
     * @return Retorna boolean caso exista um período com as mesmas
     * características.
     */
    public boolean equals(PeriodoAutorizacao outroPA) {
        if (outroPA.getDiaSemana().equals(diaSemana) && outroPA.getEquipamento().toString().equals(equipamento.toString()) && outroPA.getHoraFim().equals(hora_Fim) && outroPA.getHoraInicio().equals(hora_Inicio)) {
            return true;
        }
        return false;
    }

    /**
     * Retorna Informação sobre o Período de Autorização.
     *
     * @return Retorna o Equipamento associado, o dia da semana e as horas (de
     * inicio e fim)
     */
    public String toString() {
        return equipamento.toString() + " - " + diaSemana + " - das " + hora_Inicio + " até às " + hora_Fim;
    }

}
