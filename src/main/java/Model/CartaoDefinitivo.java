package Model;

import Registos.RegistoColaboradores;
import Utils.Data;
import java.io.Serializable;

public class CartaoDefinitivo implements Cartao {

    private String idCartao;

    private String idColaborador;

    /**
     * Construtor do cartão definitivo
     *
     * @param idCartao = id do Cartão
     * @param idColaborador = id do colaborador
     */
    public CartaoDefinitivo(String idCartao, String idColaborador) {
        this.idCartao = idCartao;
        this.idColaborador = idColaborador;
    }

    @Override
    /**
     * Método que permite obter o colaborador atribuido a um cartão
     *
     * @param data = data atual
     * @return o colaborador atribuido ao cartão em questão
     */
    public Colaborador getColaboradorAtribuido(Data data) {
        return RegistoColaboradores.getColabByID(idColaborador);
    }

    @Override
    /**
     * Método que devolve o id do Cartão em questão
     *
     * @return id do Cartão
     */
    public String getIDCartao() {
        return idCartao;
    }

}
