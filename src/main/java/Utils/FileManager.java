package Utils;

import Model.CartaoDefinitivo;
import Model.CartaoProvisorio;
import Model.Colaborador;
import Model.Empresa;
import Model.Equipamento;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class FileManager {

    /**
     * Nome do Ficheiro Binário.
     */
    private static final String FILE_NAME = "Ficheiro de Dados Bin.bin";
    
    /**
     * Verificação boolean do Ficheiro importado.
     */
    private static boolean ficheiroImportado = false;

    /**
     * Ler ficheiro a ser importado.
     * @throws Exception 
     */
    public static void ler() throws Exception {
        ler(new File(FILE_NAME));
    }

    /**
     * Método que 
     * @param nomefich
     * @throws ClassNotFoundException
     * @throws IOException 
     */
    public static void ler(File nomefich) throws ClassNotFoundException, IOException {
        ArrayList<Equipamento> registoEquip = new ArrayList<>();
        ArrayList<Colaborador> registoColab = new ArrayList<>();
        ArrayList<CartaoDefinitivo> registoCartoesD = new ArrayList<>();
        ArrayList<CartaoProvisorio> registoCartoesP = new ArrayList<>();
        ArrayList<Object> objList = new ArrayList<>();
        boolean cont = true;
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(nomefich));
        ArrayList<Object> objL = (ArrayList<Object>) in.readObject();
        for (Object objIN : objL) {
            if (objIN instanceof Equipamento) {
                registoEquip.add((Equipamento) objIN);
            } else if (objIN instanceof Colaborador) {
                System.out.println("a");
                registoColab.add((Colaborador) objIN);
            } else if (objIN instanceof CartaoProvisorio) {
                registoCartoesP.add((CartaoProvisorio) objIN);
            } else if (objIN instanceof CartaoDefinitivo) {
                registoCartoesD.add((CartaoDefinitivo) objIN);
            }
        }
        in.close();
        ficheiroImportado = true;
        Empresa.setRegistos(registoColab, registoEquip, registoCartoesP, registoCartoesD);
    }

    /**
     * Método que guarda todas as informações no ficheiro binário para mais tarde serem importadas.
     * @param registoEquip Lista de Equipamentos.
     * @param registoColab Lista de Colaboradores.
     * @param registoCartoesD Lista de Cartões Definitivos.
     * @param registoCartoesP Lista de Cartões Provisórios.
     * @throws IOException 
     */
    public static void guardar(ArrayList<Equipamento> registoEquip, ArrayList<Colaborador> registoColab, ArrayList<CartaoDefinitivo> registoCartoesD, ArrayList<CartaoProvisorio> registoCartoesP) throws IOException {
        guardar(new File(FILE_NAME), registoEquip, registoColab, registoCartoesD, registoCartoesP);
    }

    /**
     * Grava as informações de acordo com o path escolhido pelo utilizador.
     * @param nomefich Nome ficheiro pretendido
     * @param registoEquip Lista de Equipamentos.
     * @param registoColab Lista de Colaboradores.
     * @param registoCartoesD Lista de Cartões Definitivos.
     * @param registoCartoesP Lista de Cartões Provisórios.
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public static void guardar(File nomefich, ArrayList<Equipamento> registoEquip, ArrayList<Colaborador> registoColab, ArrayList<CartaoDefinitivo> registoCartoesD, ArrayList<CartaoProvisorio> registoCartoesP) throws FileNotFoundException, IOException {
        ArrayList<Object> objList = new ArrayList<>();
        for (Equipamento eq : registoEquip) {
            objList.add(eq);
        }
        for (Colaborador colab : registoColab) {
            objList.add(colab);
        }
        for (CartaoProvisorio cartP : registoCartoesP) {
            objList.add(cartP);
        }
        for (CartaoDefinitivo cartD : registoCartoesD) {
            objList.add(cartD);
        }
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(nomefich));
            out.writeObject(objList);

        } catch (IOException ex) {

        }
    }

    /**
     * Método que importa as informações do ficheiro para a aplicação.
     * @param ficheiro Ficheiro onde estão as informações
     * @throws FileNotFoundException 
     */
    public static void lerFicheiroTXT(File ficheiro) throws FileNotFoundException {
        try (Scanner in = new Scanner(ficheiro)) {
            while (in.hasNextLine()) {
                String[] linha = in.nextLine().split("/");
                Empresa.instanciarRegistos(linha);
            }
        }
        ficheiroImportado = true;
    }

    /**
     * Retorna o Ficheiro a ser importado.
     * @return ficheiro a ser importado.
     */
    public static boolean getFicheiroImportado() {
        return ficheiroImportado;
    }
}
